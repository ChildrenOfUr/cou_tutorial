<!doctype html>
<html>
    <head>
      <?php include("headtag.php"); ?>
      <title>CoU Tutorial - Playing</title>
    </head>
    <body>
      <main>
        <div id="header">
            <div>
                <a class="noa" id="title" href="#"><img src="resources/logo.svg"><span id="tutorialtitle">Tutorial</span></a>
                <ul id="progress">
                    <li class="progress-back"><a href="tutorial">Start</a></li>
                    <li class="progress-back"><a href="screenelements">Screen</a></li>
                    <li class="progress-current">Playing</li>
                    <li class="progress-next">Practice</li>
                    <li class="progress-next">Finishing Up</li>
                </ul>
                <div id="progress-current">Playing</div>
            </div>
        </div>
        <div id="content">
            <section id="playing">
                <h1>Playing the Game</h1>
                <img class="screenshot" src="resources/screenshots/screenelements_screenshot.png" />
                <p>Children of Ur has many complex elements, most of which were also in Glitch. You can buy, sell, and trade items, mine rocks, dig holes, plant (and talk to) trees, and more.</p>
                <p>If you played Glitch, you probably already know how it works. Read the tutorial to refresh your memory, or...</p>
                <p>
                  <br />
                  <span class="ct">
                    <a href="#finishingup"><center><button class="button btnbounce bounce-down">Skip to the End&emsp;<i class="fa fa-arrow-down"></i></button></a></center>
                  </span>
                </p>
            </section>
            <section id="avatarediting">
                <h1>Editing your Avatar</h1>
                <img src="resources/screenshots/screenelements_playerstatus.png" class="screenshot" />
                <p>You can edit your in-game character to look exactly like you want.</p>
                <h2><i class="fa fa-male faicon"></i> Clothing</h2>
                <p>You can use the avatar clothing page to choose clothes and try them on. Some clothes are free, and some require purchasing credits.</p>
                <h2><i class="fa fa-female faicon"></i> Accessories</h2>
                <p>In addition to clothing, you can add accesories or decorations.</p>
                <h2><i class="fa fa-tint faicon"></i> Colors</h2>
                <p>You can pick the color of your player's skin, hair, etc.</p>
                <h2><i class="fa fa-eye faicon"></i> Features</h2>
                <p>Your player's features, including eyes, nose, mouth, ears, hairstyle, etc. can be modified.</p>
            </section>
            <section id="auctions">
                <h1>Auctions</h1>
                <img class="screenshot" src="resources/screenshots/screenelements_imgmenu.png" />
                <p>Children of Ur has an auction page, where players can buy and sell items using currants.</p>
                <h2><i class="fa fa-shopping-cart faicon"></i> Buying</h2>
                <p>To buy an item, go to the auctions page, find an item with a price you like, and click the Purchase button.</p>
                <p>The item will be delivered to you by an auction delivery frog.</p>
                <h2><i class="fa fa-ticket faicon"></i> Selling</h2>
                <p>To sell an item, go to the auctions page, and click Sell Item. Choose an item and quantity from your inventory to sell, as well as a price. Currants will be given to you if/when your item is sold.</p>
                <p>If your item does not sell before the time expires, you will be given the item back, but you will not be refunded your auction fee.</p>
            </section>
            <section id="vendors">
                <h1>Vendors</h1>
                <table class="multiscreenshot">
                    <tr>
                        <td>
                            <img class="screenshotofmany" src="resources/screenshots/screenelements_property_inventorymenu.png" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img class="screenshotofmany" src="resources/screenshots/screenelements_property.png" />
                        </td>
                    </tr>
                </table>
                <p>Street vendors are creatures that hang out on certain streets, and sell items. Different types of vendors sell different selections of items.</p>
                <h2><i class="fa fa-truck faicon"></i> Buying</h2>
                <p>To buy an item from a vendor, click on it, then select they Buy tab. Choose an item, enter a quantity, and you will receive that quantity of the item, and be charged automatically.</p>
                <h2><i class="fa fa-tag faicon"></i> Selling</h2>
                <p>Sometimes, vendors will allow you to sell items. To sell an item, click on it, then select the Sell tab. Choose an item that you want to sell. If the vendor will accept the item, click Sell, and you will be given the amount of currants the vendor deems it to be worth.</p>
            </section>
            <section id="gardening">
                <h1>Gardening</h1>
                <img class="screenshot" src="resources/screenshots/screenelements_chat.png" />
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <h2><i class="fa fa-bookmark-o faicon"></i> Seeds</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <h2><i class="fa fa-th-large faicon"></i> Plots</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <h2><i class="fa fa-wrench faicon"></i> Tools</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </section>
            <section id="housesandtowers">
                <h1>Houses & Towers</h1>
                    <table class="multiscreenshot">
                      <tr>
                          <td>
                              <img class="screenshotofmany" src="resources/screenshots/screenelements_map.png" />
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <img class="screenshotofmany" src="resources/screenshots/screenelements_location.png" />
                          </td>
                      </tr>
                    </table>
                    <p>Each player gets their own street, which you can decorate with various items. Your street includes one house, and, if you build one, one tower.</p>
                    <h2><i class="fa fa-home faicon"></i> Houses</h2>
                    <p>Your house is private area that includes an indoor section (which can be expanded both horizontally and vertically), and a yard section. Expanding your house makes your yard area smaller. In it, you can place whatever items you want, and only people you give keys to (or let it when they knock) will have access to it. You can also choose a style for the outside of your house.</p>
                    <p>You can decorate it with furniture, wallpaper, flooring, etc. You can also add items, like private gardens, to the yard.</p>
                    <h2><i class="fa fa-building faicon"></i> Towers</h2>
                    <p>Towers are public buildings that you can build on your street as museums, stores, or whatever else you want. They can be decorated just like houses. They have narrower floors, but more of them, and no yard. Anyone that visits your street can get into your tower.</p>
                    <h2><i class="fa fa-child faicon"></i> Butlers</h2>
                    <p>Each player gets a customizable butler to help out on their street. Players can leave messages or items with them to give to the street's owner. You can set a message for the butler to tell every visitor, or to hand out items.</p>
                    <p>Your butler will send you a chat message when someone is on your street.</p>
                    <h2><i class="fa fa-car faicon"></i> Visiting</h2>
                    <p>Each street gets one signpole, which the owner can put up to 5 other players' streets on. Once you go to someone else's street, you can use any items they have out, but you cannot modify it. If someone lets you into their house, you will have similar abilities.</p>
                    <h2><i class="fa fa-unlock faicon"></i> Hosting</h2>
                    <p>You can let people into your house by giving them a key, or by accepting when they knock at your door.</p>
                    <h2><i class="fa fa-building-o faicon"></i> Building</h2>
                    <p>Building furniture, houses, and towers is a lot of work. Anyone can help build things on your street or tower, and only people who can access your house can help your build it.</p>
                    <h2><i class="fa fa-gift faicon"></i> Decorating</h2>
                    <p>You can decorate your house, tower, or street with furniture, items, signs, etc. You must build most of the items yourself, or buy them from other players.</p>
            </section>
            <section id="quoins">
                <h1>Quoins</h1>
                <img class="screenshot" src="resources/screenshots/screenelements_gameinfo_clock.png" />
                <p>Quoins are floating point bubbles that will give you either Imagination, Currants, Mood, or Energy when you touch them. These things are given in an amount that is calculated by your personal Quoin Multiplier, as well as the value of the individual quoin.</p>
                <h2><i class="fa fa-cloud faicon"></i> Qurazy Quoins</h2>
                <p>Qurazy Quoins are special quoins that only appear on streets you have never been to before. They give very large amounts of Imagination.</p>
            </section>
            <section id="trees">
                <h1>Trees</h1>
                <img class="screenshot" src="resources/screenshots/screenelements_gameinfo_clock.png" />
                <p>Trees are natural items that grow in patches on streets. There are several types of trees, including Bubble, Spice, Egg, Gas, Cherry, Bean, etc.</p>
                <h2><i class="fa fa-upload faicon"></i> Harvesting</h2>
                <p>To harvest a tree, click it, then select Harvest. You will be given some of its items.</p>
                <p>You can also pet or water it to help it regain these items.</p>
                <h2><i class="fa fa-sort-amount-asc faicon"></i> Planting</h2>
                <p>All trees grow from beans. To get a bean, you need to harvest one from a bean tree. Then use a bean seasoner, which you can buy from a gardening or tool vendor, to change it into the type of bean you want.</p>
                <p>To plant a bean, you need to find an open patch. Patches will become open when a tree dies (or is killed). You can either dig dirt from a patch (if you do, you have to wait for it to refill before planting), or plant a bean in it.</p>
            </section>
            <section id="mail">
                <h1>Mail</h1>
                <img class="screenshot" src="resources/screenshots/screenelements_gameinfo_clock.png" />
                <p>You can send mail to other players by using a mailbox. Mailboxes can be found on some streets, or near your house.</p>
                <h2><i class="fa fa-send faicon"></i> Sending</h2>
                <p>To send a message, click on a mailbox, then select Send Message. Choose a player to send it to, write a message, and choose to attach currants or an item.</p>
                <p>There is a fee for sending mail, which increases if you send currants or items.</p>
                <h2><i class="fa fa-envelope faicon"></i> Receiving</h2>
                <p>You can recieve some messages by Mail Delivery Frog. Other messages you can get at a mailbox.</p>
            </section>
            <section>
              <h1>To Do</h1>
              <p>Add more things.</p>
            </section>
            <section id="finishingup">          
                <h1>Moving On...</h1>
                <span class="ct">
                  <p>Now you'll get to practice playing.</p>
                  <center>
                    <a href="practice">
                      <button class="button btnbounce bounce-right">Practice&emsp;<i class="fa fa-arrow-right"></i></button>
                    </a>
                  </center>
                </span>
            </section>
        </div>
      </main>
    </body>
</html>